package lesson15;

public class VolleySport implements Sport, Team{
    String name;
    String teamName;
    @Override
    public void setName(String newName) {
        System.out.println("Acest gen de sport este " + newName);
        name = newName;
    }

    @Override
    public void getCup(String cup) {
        System.out.println("La sportul " + name + " echipa " + teamName + " luat cupa de "+ cup);
    }

    @Override
    public void buyToys(String toy) {
        System.out.println("Ati procurat " + toy);
    }

    @Override
    public void setTeamName(String newName) {
        System.out.println("Numele echipei este  " + newName);
        teamName=newName;
    }
}
