package lesson15;

class Circle extends GeometricFigur {
    @Override
    void draw() {
        System.out.println("Se deseneaza un cerc");
    }

    @Override
    void moveTo(double newX, double newY) {
        x = newX;
        y = newY;
        System.out.println(" Coordonatele geografice sun " + newX + " : " + newY);
    }

    @Override
    public double calculeazaRaza() {
        double raza = 0;
        raza = Math.sqrt((x*x) + (y*y));
        return raza;
    }
}
